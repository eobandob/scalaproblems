package com.londocr.scalaproblems

import scala.annotation.tailrec

object Lists {

  @tailrec
  def last[A](list: Seq[A]): Option[A] = {
    list match {
      case Nil => None
      case a :: Nil => Some(a)
      case _ :: tail => last(tail)
    }
  }

  @tailrec
  def penultimo[A](list: Seq[A]): Option[A] = {
    list match {
      case Nil => None
      case _ :: Nil => None
      case a :: _ :: Nil => Some(a)
      case _ :: tail => penultimo(tail)
    }
  }

  @tailrec
  def nth[A](n: Int, l: Seq[A]): Option[A] = {
    if (n < 0 || n >= l.size) None
    else l match {
      case Nil => None
      case a :: _ if n == 0 => Some(a)
      case _ :: tail => nth(n - 1, tail)
    }
  }
  
  def length[A](list: Seq[A]): Int = {
    @tailrec
    def largoRec(largo: Int, list: Seq[A]): Int = {
      list match {
        case Nil => largo
        case _ :: tail => largoRec(largo + 1, tail)
      }
    }

    largoRec(0, list)
  }

  def reverse[A](list: Seq[A]): Seq[A] = {
    @tailrec
    def reverseRec(newList: Seq[A], oldList: Seq[A]): Seq[A] = {
      oldList match {
        case Nil => newList
        case a :: tail => reverseRec(a +: newList, tail)
      }
    }
    reverseRec(Seq.empty[A], list)
  }

  def isPalindrome[A](list: Seq[A]): Boolean = {
    @tailrec
    def isPalRec(i: Int, j: Int, isItPalindrome: Boolean): Boolean = {
      if (!isItPalindrome) isItPalindrome
      else if (i == j || i - 1 == j) isItPalindrome
      else {
        val bool = list(i) == list(j)
        isPalRec(i + 1, j - 1, bool)
      }
    }
    isPalRec(0, list.size - 1, true)
  }

  def flatten(list: Seq[Any]): Seq[Any] = {
    list.flatMap {
      case x: Seq[_] => flatten(x)
      case e => List(e)
    }
  }

  def compress[A](list: Seq[A]): Seq[A] = {
    @tailrec
    def compressRec(result: Seq[A], currentlyJumping: A, oldList: Seq[A]): Seq[A] = {
      oldList match {
        case Nil => result
        case a :: tail if a == currentlyJumping => compressRec(result, currentlyJumping, tail)
        case a :: tail => compressRec(result :+ a, a, tail)
      }
    }
    if (list.isEmpty) list else compressRec(Seq(list.head), list.head, list.tail)
  }

  def pack[A](list: Seq[A]): Seq[Seq[A]] = {
    @tailrec
    def packRec(result: Seq[Seq[A]], currentlyProcessing: A, processingList: Seq[A], oldList: Seq[A]): Seq[Seq[A]] = {
      oldList match {
        case Nil => result :+ processingList
        case a :: tail if a == currentlyProcessing => packRec(result, currentlyProcessing, processingList :+ a, tail)
        case a :: tail => packRec(result :+ processingList, a, Seq(a), tail)
      }
    }

    if (list.isEmpty) Seq.empty[Seq[A]] else packRec(Seq.empty[Seq[A]], list.head, Seq(list.head), list.tail)
  }

  def encode[A](list: Seq[A]): Seq[(Int, A)] = {
    pack(list).map(l => l.size -> l.head)
  }

  def encodeModified[A](list: Seq[A]): Seq[Any] = {
    encode(list).map(t => if (t._1 == 1) t._2 else t)
  }

  def decode[A](list: Seq[(Int, A)]): Seq[A] = {
    list.flatMap(t => Seq.fill(t._1)(t._2))
  }

  def encodeDirect[A](list: Seq[A]): Seq[(Int, A)] = {
    @tailrec
    def packRec(result: Seq[(Int, A)], currentlyProcessing: A, processingCount: Int, oldList: Seq[A]): Seq[(Int, A)] = {
      oldList match {
        case Nil => result :+ (processingCount, currentlyProcessing)
        case a :: tail if a == currentlyProcessing => packRec(result, currentlyProcessing, processingCount + 1, tail)
        case a :: tail => packRec(result :+ (processingCount, currentlyProcessing), a, 1, tail)
      }
    }

    if (list.isEmpty) Seq.empty[(Int, A)] else packRec(Seq.empty[(Int, A)], list.head, 1, list.tail)
  }

  def duplicate[A](list: Seq[A]): Seq[A] = duplicateN(2, list)

  def duplicateN[A](n: Int, list: Seq[A]): Seq[A] = list.flatMap(Seq.fill(n)(_))

  def drop[A](n: Int, list: Seq[A]): Seq[A] = {
    @tailrec
    def dropRec(i: Int, seq: Seq[A]): Seq[A] = {
      if (i == 0) seq
      else seq match {
        case Nil => Nil
        case _ :: tail => dropRec(i - 1, tail)
      }
    }
    if (n > 0) dropRec(n, list) else list
  }

  def split[A](n: Int, list: Seq[A]): (Seq[A], Seq[A]) = {
    @tailrec
    def splitRec(i: Int, first: Seq[A], second: Seq[A]): (Seq[A], Seq[A]) = {
      if (i == 0) first -> second
      else second match {
        case Nil => first -> second
        case a :: tail => splitRec(i - 1, first :+ a, tail)
      }
    }
    if (n > 0) splitRec(n, Seq.empty[A], list) else Nil -> list
  }

  def slice[A](start: Int, end: Int, list: Seq[A]): Seq[A] = {
    @tailrec
    def sliceRec(s: Int, e: Int, result: Seq[A], seq: Seq[A]): Seq[A] = {
      seq match {
        case Nil => result
        case _ if s == 0 && e == 0 => result
        case _ :: tail if s > 0 => sliceRec(s - 1, e - 1, result, tail)
        case a :: tail if s == 0 && e > 0 => sliceRec(s, e - 1, result :+ a, tail)
      }
    }
    if (start > 0 && end > 0 && start <= end) sliceRec(start, end, Seq.empty[A], list) else Seq.empty[A]
  }

  def rotate[A](n: Int, list: Seq[A]): Seq[A] = {
    if (n > 0) {
      val (l1, l2) = split(n, list)
      l2 :++ l1
    } else {
      val (l1, l2) = split(list.size + n, list)
      l2 :++ l1
    }
  }

  def removeAt[A](pos: Int, list: Seq[A]): Seq[A] = {
    if (pos < 0 || pos >= list.size) list
    else {
      val (l1, l2) = split(pos , list)
      l1 ++ drop(1, l2)
    }
  }

  def insertAt[A](pos: Int, item: A, list: Seq[A]): Seq[A] = {
    val (l1, l2) = split(pos, list)
    (l1 :+ item) ++ l2
  }

  def range(from: Int, to: Int): Seq[Int] = {
    @tailrec
    def counter(current: Int, result: Seq[Int]): Seq[Int] = {
      if (current > to) result
      else counter(current + 1, result :+ current)
    }
    if (from <= to) counter(from, Seq.empty[Int]) else Seq.empty[Int]
  }

}
