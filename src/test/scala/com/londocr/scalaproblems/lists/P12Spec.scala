package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P12Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $decodeNil
      |Lista definida simple $decodeDef1
      |Lista definida procesable $decodeDef2
      |""".stripMargin

  def decodeNil = {
    val lista = List.empty[(Int, Int)]
    Lists.decode(lista) must beEmpty
  }

  def decodeDef1 = {
    val expected = List(1, 2, 3, 4, 5)
    val lista = expected.map(n => (1, n))
    Lists.decode(lista) must beEqualTo(expected)
  }

  def decodeDef2 = {
    val lista = List((4, 1), (1, 2), (2, 3), (2, 1), (1, 4), (4, 5))
    val expected = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    Lists.decode(lista) must beEqualTo(expected)
  }
}
