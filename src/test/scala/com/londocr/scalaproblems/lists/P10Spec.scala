package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P10Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $encodeNil
      |Lista definida simple $encodeDef1
      |Lista definida procesable $encodeDef2
      |""".stripMargin

  def encodeNil = {
    val lista = List.empty[Int]
    Lists.encode(lista) must beEmpty
  }

  def encodeDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = lista.map(n => (1, n))
    Lists.encode(lista) must beEqualTo(expected)
  }

  def encodeDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val expected = List((4, 1), (1, 2), (2, 3), (2, 1), (1, 4), (4, 5))
    Lists.encode(lista) must beEqualTo(expected)
  }
}
