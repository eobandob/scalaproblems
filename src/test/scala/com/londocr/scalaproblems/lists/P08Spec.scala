package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

import scala.util.Random

class P08Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $compressNil
      |Lista definida simple $compressDef1
      |Lista definida procesable $compressDef2
      |""".stripMargin

  def compressNil = {
    val lista = List.empty[Int]
    Lists.compress(lista) must beEmpty
  }

  def compressDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    Lists.compress(lista) must beEqualTo(lista)
  }

  def compressDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val expected = List(1, 2, 3, 1, 4, 5)
    Lists.compress(lista) must beEqualTo(expected)
  }
}
