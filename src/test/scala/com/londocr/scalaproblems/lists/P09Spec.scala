package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P09Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $packNil
      |Lista definida simple $packDef1
      |Lista definida procesable $packDef2
      |""".stripMargin

  def packNil = {
    val lista = List.empty[Int]
    Lists.pack(lista) must beEmpty
  }

  def packDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = lista.map(List(_))
    Lists.pack(lista) must beEqualTo(expected)
  }

  def packDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val expected = List(List(1, 1, 1, 1), List(2), List(3, 3), List(1, 1), List(4), List(5, 5, 5, 5))
    Lists.pack(lista) must beEqualTo(expected)
  }
}
