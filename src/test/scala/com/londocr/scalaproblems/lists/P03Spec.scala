package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P03Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $nthNil
      |N mayor al tamaño $nthBoundMayor
      |N negativo $nthBoundMenor
      |Lista definida $nthDef
      |Lista random $nthRandom
      |Build-in $nthScala
      |""".stripMargin

  def nthNil = {
    val lista = List.empty[Int]
    val n = 3
    Lists.nth(n, lista) must beNone
  }

  def nthBoundMayor = {
    val lista = List(1, 2, 3, 4, 5)
    val n = 10
    Lists.nth(n, lista) must beNone
  }

  def nthBoundMenor = {
    val lista = List(1, 2, 3, 4, 5)
    val n = -10
    Lists.nth(n, lista) must beNone
  }

  def nthDef = {
    val lista = List(1, 2, 3, 4, 5)
    val n = 3
    val expected = 4
    Lists.nth(n, lista) must beSome(expected)
  }

  def nthRandom = {
    val lista = Randomizer.randomIntList(10)
    val n = 10
    val expected = 5
    Lists.nth(n, lista.appended(expected)) must beSome(expected)
  }

  def nthScala = {
    val lista = Randomizer.randomIntList(10)
    val n = 3
    val expected = lista(n)
    Lists.nth(n, lista) must beSome(expected)
  }

}
